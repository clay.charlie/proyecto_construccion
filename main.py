from builtins import print

from datos import DataBase
from Tipo_datos import usuario
from Tipo_datos import recurso
from Tipo_datos import tipo_recurso
from Tipo_datos import tutor

conexion = DataBase()

recurso1= conexion.get_recurso(1)
tutor1= conexion.get_tutor(recurso1[2])
usuario1 = conexion.get_usuario(tutor1[1])
tipo_recurso1= conexion.get_tipo_recurso(recurso1[1])

conexion.create_tutor(1,4000.0,"123409872345",usuario1[0])
conexion.creacion_tipo_recurso(1,"talleres","ejercicios de un tema")
conexion.creacion_recurso(1,1,1,3000.0,"ejercicios de ecuaciones diferenciales")


print(f'el recurso N° = {recurso1[0]} , creador = {usuario1[1]} ,descripcion = {recurso1[4]}, valor ={recurso1[3]}, tipo = {tipo_recurso1[1]}')
