import pymysql
from Tipo_datos import estudiante
from Tipo_datos import tutor
from Tipo_datos import usuario

class DataBase:

    def __init__(self):
        self.connection= pymysql.connect(
            host='localhost',
            user='root',
            password='',
            db='db_tutores'
        )
        self.cursor = self.connection.cursor()
        print('conexion exitosa')

    def get_usuario(self,id):
        sql = f'select id_usuario , nombre , email, numero from usuario where id_usuario = {id}'
        try:
            self.cursor.execute(sql)
            usuario = self.cursor.fetchone()
            return usuario
        except Exception as e:
            raise

    def create_usuario(self,id_usuario,nombre,email,numero):
        sql=f'insert into usuario (`id_usuario`, `nombre`, `email`, `numero`) VALUES ({id_usuario},\'{nombre}\',\'{email}\',\'{numero}\')'
        try:
            self.cursor.execute(sql)
            self.connection.commit()
        except Exception as e:
            raise

    def create_tutor(self,id_tutor,costo_tutoria,cuenta_bancaria,id_usuario):
        sql = f'INSERT INTO `tutor`(`id_tutor`, `id_usuario`, `costo_tutoria`, `cuenta_bancaria`) VALUES ({id_tutor},{id_usuario},{costo_tutoria},\'{cuenta_bancaria}\')'
        try:
            self.cursor.execute(sql)
            self.connection.commit()
        except Exception as e:
            raise

    def get_tutor(self,id_tutor):
        sql= f'SELECT `id_tutor`, `id_usuario`, `costo_tutoria`, `cuenta_bancaria` FROM `tutor` WHERE id_tutor = {id_tutor}'
        try:
            self.cursor.execute(sql)
            tutor = self.cursor.fetchone()
            return tutor
        except Exception as e:
            raise

    def creacion_recurso(self,id_recurso,id_tipo_recurso,id_tutor,valor,descripcion):
        sql=f'INSERT INTO `recursos`(`id_recurso`, `id_tipo_recurso`, `id_tutor`, `valor`, `descripcion`) VALUES ({id_recurso},{id_tipo_recurso},{id_tutor},{valor},\'{descripcion}\')'
        try:
            self.cursor.execute(sql)
            self.connection.commit()
        except Exception as e:
            raise

    def get_recurso(self, id_recurso):
        sql = f'SELECT `id_recurso`, `id_tipo_recurso`, `id_tutor`, `valor`, `descripcion` FROM `recursos` WHERE id_recurso = {id_recurso}'
        try:
            self.cursor.execute(sql)
            recurso = self.cursor.fetchone()
            return recurso
        except Exception as e:
            raise

    def creacion_tipo_recurso(self,id_tipo_recurso,nombre_tipo,descripcion_tipo):
        sql=f'INSERT INTO `tipo_recursos`(`id_tipo_recursos`, `nombre_tipo`, `descripcion_tipo`) VALUES ({id_tipo_recurso},\'{nombre_tipo}\',\'{descripcion_tipo}\')'
        try:
            self.cursor.execute(sql)
            self.connection.commit()
        except Exception as e:
            raise


    def get_tipo_recurso(self, id_tipo_recurso):
        sql = f'SELECT `id_tipo_recursos`, `nombre_tipo`, `descripcion_tipo` FROM `tipo_recursos` WHERE id_tipo_recursos = {id_tipo_recurso}'
        try:
            self.cursor.execute(sql)
            tipo_recurso = self.cursor.fetchone()
            return tipo_recurso
        except Exception as e:
            raise