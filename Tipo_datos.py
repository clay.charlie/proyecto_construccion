class usuario:
    def __init__(self ,id_usuario , nombre ,email ,numero):
        self.id_usuario = id_usuario
        self.nombre = nombre
        self.email = email
        self.numero = numero

    def get_id(self):
        return self.id_usuario

    def __str__(self):
        return f'id={self.id} , nombre= {self.nombre} , email = {self.email} , numero = {self.numero}'


class estudiante (usuario):
    def __init__(self,id_usuario,nombre,email,numero,id_estudiante,cuenta_bancaria):
        self.id_estudiante=id_estudiante
        self.cuenta_bancaria=cuenta_bancaria
        usuario.__str__(self,id_usuario,nombre,email,numero)

    def __str__(self):
        return f'id_usuario={self.id_usuario} , nombre= {self.nombre} , email = {self.email} , numero = {self.numero} , ' \
               f'id_estudiante ={self.id_estudiante} , cuenta bancaria = {self.cuenta_bancaria}'

class tutor (usuario):
    def __init__(self,id_usuario,nombre,email,numero,id_tutor,costo_tutoria,cuenta_bancaria):
        self.id_tutor=id_tutor
        self.costo_tutoria=costo_tutoria
        self.cuenta_bancaria=cuenta_bancaria
        usuario.__str__(self, id_usuario, nombre, email, numero)

    def __str__(self):
      return f'id_usuario={self.id_usuario} , nombre= {self.nombre} , email = {self.email} , numero = {self.numero} , ' \
               f'id_tutor ={self.id_tutor} , cuenta bancaria = {self.cuenta_bancaria} , costo tutoria = {self.costo_tutoria} '

class recurso:
    def __init__(self,id_recurso,id_tipo_recurso,id_tutor,valor,descripcion):
        self.id_recurso = id_recurso
        self.id_tipo_recurso=id_tipo_recurso
        self.id_tutor = id_tutor
        self.valor=valor
        self.descripcion=descripcion

    def __str__(self):
        return f'id_recurso = {self.id_recurso}, id_tipo_recurso = {self.id_tipo_recurso} , id_tutor = {self.id_tutor} ,valor ={self.valor} , descripción {self.descripcion}'


class tipo_recurso:
    def __init__(self,id_tipo_recurso,nombre_tipo,descripcion_tipo):
        self.id_tipo_recurso=id_tipo_recurso
        self.nombre_tipo=nombre_tipo
        self.descripcion_tipo=descripcion_tipo

    def __str__(self):
        return f'id_tipo_recurso ={self.id_tipo_recurso}, nombre tipo = {self.nombre_tipo}, descripcion tipo = {self.descripcion_tipo}'